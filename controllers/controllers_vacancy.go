package controllers

import (
	"encoding/json"
	"fmt"
	"github.com/go-chi/chi"
	"net/http"
	"strconv"

	"gitlab.com/citaces/docker-kata/selenium"
	"gitlab.com/citaces/docker-kata/service"
)

type VacancyController interface {
	CreateVacancies(w http.ResponseWriter, r *http.Request)
	DeleteVacancy(w http.ResponseWriter, r *http.Request)
	GetVacancy(w http.ResponseWriter, r *http.Request)
	ListVacancies(w http.ResponseWriter, r *http.Request)
}

type VacancyControl struct {
	service service.Service
}

func NewVacControl(service service.Service) *VacancyControl {
	return &VacancyControl{service}
}

func (u *VacancyControl) CreateVacancies(w http.ResponseWriter, r *http.Request) {
	category := chi.URLParam(r, "category")
	s := selenium.NewSelenium(category)
	res, err := s.Run()
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	for _, vac := range res {
		err = u.service.CreateDTO(vac)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}
	}
	_, err = fmt.Fprintf(w, "successful, 200 OK!")
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (u *VacancyControl) DeleteVacancy(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "id")
	n, err := strconv.Atoi(id)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	err = u.service.DeleteDTO(n)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	_, err = fmt.Fprintf(w, "successful, 200 OK!")
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (u *VacancyControl) GetVacancy(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "id")
	n, err := strconv.Atoi(id)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	vacancy, err := u.service.GetDTOByID(n)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	w.Header().Set("Content-Type", "application/json;charset=utf-8")

	err = json.NewEncoder(w).Encode(vacancy)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

}

func (u *VacancyControl) ListVacancies(w http.ResponseWriter, r *http.Request) {
	vacancies, err := u.service.GetListDTO()
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	err = json.NewEncoder(w).Encode(vacancies)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}
