package service

import (
	"gitlab.com/citaces/docker-kata/models"
	"gitlab.com/citaces/docker-kata/repository"
)

type Service interface {
	CreateDTO(dto models.Vacancy) error
	GetDTOByID(id int) (models.Vacancy, error)
	GetListDTO() ([]models.Vacancy, error)
	DeleteDTO(id int) error
}

type Serv struct {
	repository repository.Repository
}

func NewServ(repo repository.Repository) *Serv {
	return &Serv{
		repo,
	}
}

func (s *Serv) CreateDTO(dto models.Vacancy) error {
	return s.repository.Create(dto)
}
func (s *Serv) GetDTOByID(id int) (models.Vacancy, error) {
	return s.repository.GetByID(id)
}
func (s *Serv) GetListDTO() ([]models.Vacancy, error) {
	return s.repository.GetList()
}
func (s *Serv) DeleteDTO(id int) error {
	return s.repository.Delete(id)
}
