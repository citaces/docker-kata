package repository

import (
	"errors"

	"gitlab.com/citaces/docker-kata/models"
)

type Repository interface {
	Create(dto models.Vacancy) error
	GetByID(id int) (models.Vacancy, error)
	GetList() ([]models.Vacancy, error)
	Delete(id int) error
}

type Repo struct {
	vac map[int]*models.Vacancy
	IDx int
}

func NewRepo() *Repo {
	return &Repo{
		make(map[int]*models.Vacancy),
		1,
	}
}

func (r *Repo) Create(dto models.Vacancy) error {
	dto.ID = r.IDx
	r.vac[dto.ID] = &dto
	r.IDx++
	return nil
}
func (r *Repo) GetByID(id int) (models.Vacancy, error) {
	if val, ok := r.vac[id]; ok {
		return *val, nil
	}
	return models.Vacancy{}, errors.New("not found")
}
func (r *Repo) GetList() ([]models.Vacancy, error) {
	var res []models.Vacancy
	for _, elem := range r.vac {
		res = append(res, *elem)
	}
	if res != nil {
		return res, nil
	}
	return []models.Vacancy{}, errors.New("no vacancies available")
}
func (r *Repo) Delete(id int) error {
	if _, ok := r.vac[id]; ok {
		delete(r.vac, id)
	} else {
		return errors.New("wrong id, vacancy not found")
	}
	return nil
}
