package main

import (
	"gitlab.com/citaces/docker-kata/controllers"
	"gitlab.com/citaces/docker-kata/handlers"
	"gitlab.com/citaces/docker-kata/repository"
	"gitlab.com/citaces/docker-kata/service"
)

func main() {
	repo := repository.NewRepo()
	serv := service.NewServ(repo)
	control := controllers.NewVacControl(serv)
	handler := handlers.NewHandlers(control)
	handler.Route()
}
