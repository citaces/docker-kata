package models

type Vacancy struct {
	ID          int    `json:"id"`
	Context     string `json:"@context"`
	Type        string `json:"@type"`
	DatePosted  string `json:"datePosted"`
	Title       string `json:"title"`
	Description string `json:"description"`
	//Identifier         Identifier         `json:"identifier"`
	ValidThrough string `json:"validThrough"`
	//HiringOrganization HiringOrganization `json:"hiringOrganization"`
	//JobLocation        JobLocation        `json:"jobLocation"`
	//JobLocationType    string             `json:"jobLocationType"`
	//EmploymentType     string             `json:"employmentType"`
}

type Identifier struct {
	Type  string `json:"@type"`
	Name  string `json:"name"`
	Value string `json:"value"`
}

type HiringOrganization struct {
	Type   string `json:"@type"`
	Name   string `json:"name"`
	Logo   string `json:"logo"`
	SomeAs string `json:"someAs"`
}

type JobLocation struct {
	Type    string  `json:"@type"`
	Address Address `json:"address"`
}

type Address struct {
	Type            string         `json:"@type"`
	StreetAddress   string         `json:"streetAddress"`
	AddressLocality string         `json:"addressLocality"`
	AddressCountry  AddressCountry `json:"addressCountry"`
}

type AddressCountry struct {
	Type string `json:"@type"`
	Name string `json:"name"`
}
