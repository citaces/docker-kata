package public

import "gitlab.com/citaces/docker-kata/models"

// swagger:route POST /search/{category} vacancy vacanciesSearchRequest
// Creates list of vacancy.
// responses:
// 200: vacanciesSearchResponse
//  400: description: Bad request
//	500: description: Internal server error

// swagger:parameters vacanciesSearchRequest
//
//nolint:all
type vacanciesSearchRequest struct {
	//Category of vacancy
	//required:true
	//in:path
	Category string `json:"category"`
}

// swagger:response vacanciesSearchResponse
//
//nolint:all
type vacanciesSearchResponse struct {
	Status string
}

// swagger:route POST /delete/{id} vacancy vacancyDelByIdRequest
// Deletes the vacancy for the selected id.
// responses:
// 200: vacancyDelByIdResponse
//  400: description: Bad request
//	500: description: Internal server error

// swagger:parameters vacancyDelByIdRequest
//
//nolint:all
type vacancyDelByIdRequest struct {
	//Category of vacancy
	//required:true
	//in:path
	ID int `json:"id"`
}

// swagger:response vacancyDelByIdResponse
//
//nolint:all
type vacancyDelByIdResponse struct {
	//in:body
	Status string
}

// swagger:route POST /get/{id} vacancy vacancyGetByIdRequest
// Get the vacancy for the selected id.
// responses:
// 200: vacancyGetByIdResponse
//  400: description: Bad request
//	500: description: Internal server error

// swagger:parameters vacancyGetByIdRequest
//
//nolint:all
type vacancyGetByIdRequest struct {
	//id of vacancy
	//required:true
	//in:path
	ID int `json:"id"`
}

// swagger:response vacancyGetByIdResponse
//
//nolint:all
type vacancyGetByIdResponse struct {
	//in:body
	Body models.Vacancy
}
