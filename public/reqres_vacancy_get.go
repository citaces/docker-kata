package public

import (
	"gitlab.com/citaces/docker-kata/models"
)

// swagger:route GET /list vacancy vacanciesGetListRequest
// List of all available vacancies.
// responses:
// 200: vacanciesGetListResponse
//  400: description: Bad request
//	500: description: Internal server error

// swagger:parameters vacanciesGetListRequest
//
//nolint:all

// swagger:response vacanciesGetListResponse
//
//nolint:all
type vacanciesGetListResponse struct {
	//in:body
	Body []models.Vacancy
}
