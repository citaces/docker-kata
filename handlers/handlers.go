package handlers

import (
	"context"
	"gitlab.com/citaces/docker-kata/controllers"
	"gitlab.com/citaces/docker-kata/swaggerui"
	"log"
	"net/http"
	"os"
	"os/signal"
	"time"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
)

type Handlers struct {
	vacancyControllers controllers.VacancyController
}

func NewHandlers(vacancyController controllers.VacancyController) *Handlers {
	return &Handlers{
		vacancyControllers: vacancyController,
	}
}

func (h *Handlers) Route() {
	port := ":8080"

	r := chi.NewRouter()
	r.Use(middleware.Logger)
	r.Post("/search/{category}", h.vacancyControllers.CreateVacancies)
	r.Post("/delete/{id}", h.vacancyControllers.DeleteVacancy)
	r.Post("/get/{id}", h.vacancyControllers.GetVacancy)
	r.Get("/list", h.vacancyControllers.ListVacancies)

	r.Get("/swagger", swaggerui.SwaggerUI)
	r.Get("/public/*", func(w http.ResponseWriter, r *http.Request) {
		http.StripPrefix("/public/",
			http.FileServer(http.Dir("./public"))).ServeHTTP(w, r)
	})
	srv := &http.Server{
		Addr:    port,
		Handler: r,
	}
	go func() {
		log.Printf("server started on port %s", port)
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatalf("listen: %s\n", err)
		}
	}()
	quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt)
	<-quit
	log.Println("Shutdown Server ...")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	if err := srv.Shutdown(ctx); err != nil {
		log.Fatal("Server Shutdown:", err)
	}
	log.Println("Server exiting")
}
